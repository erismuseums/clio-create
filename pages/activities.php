<div class="title">Manage Activities</div>
<div class="button_bar">
  <div class="left">
	  <div class="button" id="create_activity">
	    <div class="icon">
	      <span class="material-icons">add</span>
	    </div>
	    <div class="text">Create Activity</div>
	  </div>
	  <div class="button" id="create_exhibit">
	    <div class="icon">
	      <span class="material-icons">view_module</span>
	    </div>
	    <div class="text">Create Exhibit</div>
	  </div>
	</div>
	<div class="right">
		<div class="button" id="view_hidden">
	      <div class="icon">
	        <span class="material-icons">visibility</span>
	      </div>
	      <div class="text">View All</div>
	    </div>
      <div class="button small">
      <div class="icon">
        <span class="material-icons">filter_alt</span>
      </div>
      <div class="text">Filter</div>
    </div>
          <div style="height:40px;width:10px;"></div>
	    <select style="min-width: 400px" class="dropdown bar_sort">
	        <option value="nameAscending">Name Ascending</option>
	        <option value="nameDescending">Name Descending</option>
	        <option value="dateAscending">Date Ascending</option>
	        <option value="dateDescending">Date Descending</option>
	    </select>
	</div>
</div>
<div class="list_content hidden_selected" id="activities">
  
	<div class="action_card exhibit">
    <div class="preview" style="background-image: url('./img/placeholder1.jpeg')">
      <div class="badge">
        <span class="material-icons">view_module</span>
      </div>
      <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_exhibit" data-id="1">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_exhibit" data-id="1">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_exhibit" data-id="1">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card user_hidden">
    <div class="preview" style="background-image: url('./img/placeholder2.jpeg')">
      <div class="badge"></div>
      <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_activity" data-id="2">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_activity" data-id="2">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_activity" data-id="2">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card">
    <div class="preview" style="background-image: url('./img/placeholder3.jpeg')">
      <div class="badge"></div>
      <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_activity" data-id="3">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_activity" data-id="3">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_activity" data-id="3">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card">
    <div class="preview" style="background-image: url('./img/placeholder4.jpeg')">
      <div class="badge"></div>
      <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_activity" data-id="4">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_activity" data-id="4">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_activity" data-id="4">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card user_hidden exhibit">
    <div class="preview" style="background-image: url('./img/placeholder5.jpeg')">
      <div class="badge">
        <span class="material-icons">view_module</span>
      </div>
      <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_exhibit" data-id="5">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_exhibit" data-id="5">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_exhibit" data-id="5">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card">
    <div class="preview" style="background-image: url('./img/placeholder6.jpeg')">
      <div class="badge"></div>
      <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_activity" data-id="6">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_activity" data-id="6">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_activity" data-id="6">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>




  <div class="action_card exhibit">
    <div class="preview" style="background-image: url('./img/placeholder1.jpeg')">
      <div class="badge">
        <span class="material-icons">view_module</span>
      </div>
      <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_exhibit" data-id="7">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_exhibit" data-id="7">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_exhibit" data-id="7">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card user_hidden">
    <div class="preview" style="background-image: url('./img/placeholder2.jpeg')">
      <div class="badge"></div>
      <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_activity" data-id="8">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_activity" data-id="8">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_activity" data-id="8">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card">
    <div class="preview" style="background-image: url('./img/placeholder3.jpeg')">
      <div class="badge"></div>
      <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_activity" data-id="9">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_activity" data-id="9">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_activity" data-id="9">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card exhibit">
    <div class="preview" style="background-image: url('./img/placeholder4.jpeg')">
      <div class="badge">
        <span class="material-icons">view_module</span>
      </div>
      <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_exhibit" data-id="10">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_exhibit" data-id="10">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_exhibit" data-id="10">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card">
    <div class="preview" style="background-image: url('./img/placeholder5.jpeg')">
      <div class="badge"></div>
      <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_activity" data-id="11">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_activity" data-id="11">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_activity" data-id="11">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card">
    <div class="preview" style="background-image: url('./img/placeholder6.jpeg')">
     <div class="badge"></div>
     <div class="button_bar">
        
        
                <div class="tool_button action_button" data-action="edit_activity" data-id="12">
          <span class="material-icons">edit</span>
        </div>
        <div class="tool_button action_button" data-action="preview_activity" data-id="12">
          <span class="material-icons">preview</span>
        </div>
      </div>
    </div>
    <div class="title_bar action_button" data-action="view_activity" data-id="12">
      <div class="title">
        Test
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>


</div>
