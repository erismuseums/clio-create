<div class="title">Extensions</div>

<div class="list_content" id="extensions">
  
  <div class="action_card">
    <div class="preview">
      <span class="material-icons">dashboard_customize</span>
    </div>
    <div class="title_bar system_button"  data-action="activitytypes">
      <div class="title">
        Activity Types
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card">
    <div class="preview">
      <span class="material-icons">table_rows</span>
    </div>
    <div class="title_bar system_button"  data-action="menus">
      <div class="title">
        Menus
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

 
</div>
