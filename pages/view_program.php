<div class="title">
  <div id="back">
    <span class="material-icons">arrow_back</span>
  </div>
  View Program
</div>
<div class="button_bar">
  
  <div class="left">
    
    <div class="button action_button" data-action="edit_program" id="editProgram">
      <div class="icon">
        <span class="material-icons">edit</span>
      </div>
      <div class="text">Edit</div>
    </div>
  </div>
  <div class="right">

    <div class="button action_button" data-action="archive_program" id="archiveProgram">
      <div class="icon">
        <span class="material-icons">archive</span>
      </div>
      <div class="text">Archive</div>
    </div>

    <div class="button action_button red" data-action="delete_program" id="deleteExhibit">
      <div class="icon">
        <span class="material-icons">delete</span>
      </div>
      <div class="text">Delete</div>
    </div>

  </div>
</div>
<div class="list_content" id="editProgram">
  
	
  Test
  
</div>
