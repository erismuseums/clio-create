<div class="title">
  <div id="back">
    <span class="material-icons">arrow_back</span>
  </div>
  Create Exhibit
</div>

<div class="list_content" id="editExhibit">
  
	
  Test
  <select style="min-width: 400px" class="dropdown" name="setting__contrast">
                <option value="default">Use default themes</option>
                <option value="blackonwhite">Black text on a light background</option>
                <option value="whiteonblack">White text on a dark background</option>
                <option value="greenonblack">Green text on a dark background</option>
              </select>
  
</div>
<div class="button_bar bottom">
  
  <div class="left">
    
    <div class="button green action_button" data-action="save_exhibit" id="saveExhibit">
      <div class="icon">
        <span class="material-icons">save</span>
      </div>
      <div class="text">Save</div>
    </div>

    <div class="button action_button" data-action="cancel_edit" id="cancelEdit">
      <div class="icon">
        <span class="material-icons">cancel</span>
      </div>
      <div class="text">Cancel</div>
    </div>

  </div>
  <div class="right">

    

  </div>
</div>