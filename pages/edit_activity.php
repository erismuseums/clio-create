<div class="title">
  <div id="back">
    <span class="material-icons">arrow_back</span>
  </div>
  Manage Activity
</div>

<div class="list_content" id="editActivity">
  
	
  <ul class="nav nav-pills" id="program_settings" role="tablist">
    <li class="nav-item" role="presentation">
      <button class="nav-link active" id="general-tab" data-bs-toggle="tab" data-bs-target="#general" type="button" role="tab" aria-controls="general" aria-selected="true">General</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link" id="tags-tab" data-bs-toggle="tab" data-bs-target="#tags" type="button" role="tab" aria-controls="style" aria-selected="false">Tags</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link" id="media-tab" data-bs-toggle="tab" data-bs-target="#media" type="button" role="tab" aria-controls="style" aria-selected="false">Media</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link" id="activitycontent-tab" data-bs-toggle="tab" data-bs-target="#activitycontent" type="button" role="tab" aria-controls="style" aria-selected="false">Content</button>
    </li>
  </ul>
  <div class="tab-content" id="myTabContent">
    
    <div class="tab-pane show active" id="general" role="tabpanel" aria-labelledby="general-tab">


      <div class="input_form">
        <div class="form_row single">
          <div class="title">
            <span>Title:</span>
          </div>
          <div class="input">
            
            <input type="text" id="title" name="title">

          </div>
        </div>

        <div class="form_row single">
          <div class="title">
            <span>Activity Type:</span>
          </div>
          <div class="input">
            
            <select style="min-width: 400px" class="dropdown">
              <option value="annotated_image">Annotated Image</option>
              <option value="slideshow">Slideshow</option>
              <option value="visual_thinking">Visual Thinking</option>
              <option value="pathfinder">Pathfinder</option>
            </select>

          </div>
        </div>

        <div class="form_row single">
          <div class="title">
            <span>Audience</span>
          </div>
          <div class="input">
            
            <select style="min-width: 400px" class="dropdown">
              <option value="audience1">Audience 1</option>
              <option value="audience2">Audience 2</option>
              <option value="audience3">Audience 3</option>
              <option value="audience4">Audience 4</option>
            </select>

          </div>
        </div>

        <div class="form_row field">
          <div class="title">
            <span>Description:</span>
          </div>
          <div class="input">
            
            <textarea>Test</textarea>
            
          </div>
        </div>

        <div class="form_row single">
          <div class="title">
            <span>Theme:</span>
          </div>
          <div class="input">
            
            <select style="min-width: 400px" class="dropdown">
              <option value="default">Default</option>
              <option value="green">Green</option>
              <option value="red">Red</option>
              <option value="blue">Blue</option>
            </select>

          </div>
        </div>

        <div class="form_row optional">
          <div class="title">
            <span>Hide from Facilitator Mode:</span>
          </div>
          <div class="input">
            
            <input type="checkbox" id="hidden_activity" name="hidden_activity">

          </div>
        </div>


      </div>


    </div>
    <div class="tab-pane" id="tags" role="tabpanel" aria-labelledby="tags-tab">
      

      <div class="input_form">
        <div id="included_list">
          <div class="list_header">
            Included Tags
          </div>
          <div class="list_row">
            <div class="icon red">
              <span class="material-icons">remove_circle</span>
            </div>
            <div class="title">
              Tag 1
            </div>
          </div>

          <div class="list_row">
            <div class="icon red">
              <span class="material-icons">remove_circle</span>
            </div>
            <div class="title">
              Tag 2
            </div>
          </div>

          <div class="list_row">
            <div class="icon red">
              <span class="material-icons">remove_circle</span>
            </div>
            <div class="title">
              Tag 3
            </div>
          </div>

          <div class="list_row">
            <div class="icon green">
              <span class="material-icons">add_circle</span>
            </div>
            <div class="title">
              Include a Tag
            </div>
          </div>
        </div>

      </div>


    </div>
    
    <div class="tab-pane" id="media" role="tabpanel" aria-labelledby="media-tab">
      
      <div class="input_form">
        <div class="form_row field">
          <div class="title">
            <span>Media</span>
          </div>
          <div class="input">
            
            <textarea>Media</textarea>
            
          </div>
        </div>
      </div>
    </div>

    <div class="tab-pane" id="activitycontent" role="tabpanel" aria-labelledby="activitycontent-tab">
      
      <div class="input_form">
        <div class="form_row field">
          <div class="title">
            <span>Content</span>
          </div>
          <div class="input">
            
            <textarea>Content</textarea>
            
          </div>
        </div>
      </div>


    </div>
  </div>
  
</div>
<div class="button_bar bottom">
  
  <div class="left">
    
    <div class="button green action_button" data-action="save_activity" id="saveExhibit">
      <div class="icon">
        <span class="material-icons">save</span>
      </div>
      <div class="text">Save</div>
    </div>

    <div class="button action_button" data-action="cancel_edit" id="cancelEdit">
      <div class="icon">
        <span class="material-icons">cancel</span>
      </div>
      <div class="text">Cancel</div>
    </div>

  </div>
  <div class="right">

    <div class="button action_button" data-action="archiv_activity" id="archiveExhibit">
      <div class="icon">
        <span class="material-icons">archive</span>
      </div>
      <div class="text">Archive</div>
    </div>

    <div class="button red action_button" data-action="delete_activity" id="deleteExhibit">
      <div class="icon">
        <span class="material-icons">delete</span>
      </div>
      <div class="text">Delete</div>
    </div>

  </div>
</div>