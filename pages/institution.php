<div class="title">Institution</div>

<div class="list_content" id="institution">
  
<div class="action_card">
    <div class="preview">
      <span class="material-icons">account_balance</span>
    </div>
    <div class="title_bar system_button" data-action="general">
      <div class="title">
        General
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card">
    <div class="preview">
      <span class="material-icons">group</span>
    </div>
    <div class="title_bar system_button" data-action="audiences">
      <div class="title">
        Audiences
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card">
    <div class="preview">
      <span class="material-icons">palette</span>
    </div>
    <div class="title_bar system_button" data-action="themes">
      <div class="title">
        Themes
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>

  <div class="action_card">
    <div class="preview">
      <span class="material-icons">perm_media</span>
    </div>
    <div class="title_bar system_button" data-action="media">
      <div class="title">
        Media
      </div>  
      <div class="arrow">
        <span class="material-icons">arrow_forward</span>
      </div>
    </div>
  </div>



</div>
