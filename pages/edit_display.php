<div class="title">
  <div id="back">
    <span class="material-icons">arrow_back</span>
  </div>
  Manage Display
</div>

<div class="list_content" id="editExhibit">
  
  <ul class="nav nav-pills" id="program_settings" role="tablist">
    <li class="nav-item" role="presentation">
      <button class="nav-link active" id="general-tab" data-bs-toggle="tab" data-bs-target="#general" type="button" role="tab" aria-controls="general" aria-selected="true">General</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link" id="programs-tab" data-bs-toggle="tab" data-bs-target="#programs" type="button" role="tab" aria-controls="programs" aria-selected="false">Programs</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link" id="style-tab" data-bs-toggle="tab" data-bs-target="#style" type="button" role="tab" aria-controls="style" aria-selected="false">Style</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link" id="media-tab" data-bs-toggle="tab" data-bs-target="#media" type="button" role="tab" aria-controls="media" aria-selected="false">Media</button>
    </li>
  </ul>
  <div class="tab-content" id="myTabContent">
    
    <div class="tab-pane show active" id="general" role="tabpanel" aria-labelledby="general-tab">


      <div class="input_form">
        <div class="form_row single">
          <div class="title">
            <span>Title:</span>
          </div>
          <div class="input">
            
            <input type="text" id="title" name="title">

          </div>
        </div>

        <div class="form_row single">
          <div class="title">
            <span>Resolution:</span>
          </div>
          <div class="input">
            
            <select style="min-width: 400px" class="dropdown">
              <option value="automatic">Automatic</option>
              <option value="1920x1080">1080p</option>
              <option value="1280x720">720p</option>
              <option value="other">Other</option>
            </select>

          </div>
        </div>

        <div class="form_row field">
          <div class="title">
            <span>Description:</span>
          </div>
          <div class="input">
            
            <textarea>Test</textarea>
            
          </div>
        </div>
      </div>


    </div>
    <div class="tab-pane" id="programs" role="tabpanel" aria-labelledby="programs-tab">
      

      <div class="input_form">
        <div id="included_list">
          
          <div class="list_header">
            Included Programs
          </div>
          <div class="list_row">
            <div class="icon red">
              <span class="material-icons">remove_circle</span>
            </div>
            <div class="title">
              Program 1
            </div>
          </div>

          <div class="list_row">
            <div class="icon red">
              <span class="material-icons">remove_circle</span>
            </div>
            <div class="title">
              Program 2
            </div>
          </div>

          <div class="list_row">
            <div class="icon red">
              <span class="material-icons">remove_circle</span>
            </div>
            <div class="title">
              Program 3
            </div>
          </div>

          <div class="list_row">
            <div class="icon green">
              <span class="material-icons">add_circle</span>
            </div>
            <div class="title">
              Include a Program
            </div>
          </div>
        </div>

      </div>


    </div>
    
    <div class="tab-pane" id="style" role="tabpanel" aria-labelledby="style-tab">
      
      <div class="input_form">
        <div class="form_row field">
          <div class="title">
            <span>Custom Stylesheets:</span>
          </div>
          <div class="input">
            
            <textarea>Stylesheet</textarea>
            
          </div>
        </div>
      </div>

    </div>
    <div class="tab-pane" id="media" role="tabpanel" aria-labelledby="media-tab">
      
      <div class="input_form">
        <div class="form_row field">
          <div class="title">
            <span>Media</span>
          </div>
          <div class="input">
            
            <textarea>Media</textarea>
            
          </div>
        </div>
      </div>
    </div>
  </div>

  
  	
  

  
  
</div>
<div class="button_bar bottom">
  
  <div class="left">
    
    <div class="button green action_button" data-action="save_display" id="saveExhibit">
      <div class="icon">
        <span class="material-icons">save</span>
      </div>
      <div class="text">Save</div>
    </div>

    <div class="button action_button" data-action="cancel_edit" id="cancelEdit">
      <div class="icon">
        <span class="material-icons">cancel</span>
      </div>
      <div class="text">Cancel</div>
    </div>

  </div>
  <div class="right">

    <div class="button action_button" data-action="archive_display" id="archiveExhibit">
      <div class="icon">
        <span class="material-icons">archive</span>
      </div>
      <div class="text">Archive</div>
    </div>

    <div class="button red action_button" data-action="delete_exhibit" id="deleteExhibit">
      <div class="icon">
        <span class="material-icons">delete</span>
      </div>
      <div class="text">Delete</div>
    </div>

  </div>
</div>