$(document).ready(function(){
	var url = getCurrentURL();
	
	if(url['mode'] != undefined && url['type'] != undefined && url['id'] != undefined && url['menu'] != undefined){
		
		var menu_section = {};
		menu_section['menu'] = url['menu'];

		removeSelectorClassByPrefix("#sidebar", "selected_")
		$("#sidebar").addClass("selected_" + url["menu"]);

		setURLParameters(menu_section,url["type"],url['mode'],url['id']);	
		loadPage(url['mode'] + "_" + url['type']);

	} else

	if (url['menu'] != undefined){

		loadPage(url['menu']);
		var menu = url['menu'].split("/")
		removeSelectorClassByPrefix("#sidebar", "selected_")
		$("#sidebar").addClass("selected_" + menu[0]);

	} else

	if(url['menu'] == undefined){

		setURLParameters("displays");
		loadPage("displays");
		$("#sidebar").addClass("selected_displays");

	}

	var sidebar = getLocal("sidebar","full");
	
	if(sidebar == "collapsed"){
		$("#sidebar").removeClass("full");
		$("#sidebar").addClass("collapsed");
		$("#view_button").removeClass("full");
		$("#view_button").addClass("collapsed");
		$("#view_button span").html("chevron_right");
	} else {
		$("#sidebar").removeClass("collapsed");
		$("#sidebar").addClass("full");
		$("#view_button").removeClass("collapsed");
		$("#view_button").addClass("full");
		$("#view_button span").html("chevron_left")
	}
	
	
	$("#sidebar").show()


})