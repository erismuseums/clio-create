$(document).ready(function(){

	$(document).on("click","#view_button",function(){
		if($(this).hasClass("full")){
			$("#sidebar").removeClass("full");
			$("#sidebar").addClass("collapsed");
			setLocal("sidebar","collapsed")
			$("#view_button").removeClass("full");
			$("#view_button").addClass("collapsed");
			$("#view_button span").html("chevron_right");

		} else

		if($(this).hasClass("collapsed")){
			$("#sidebar").removeClass("collapsed");
			$("#sidebar").addClass("full");
			setLocal("sidebar","full")
			$("#view_button").removeClass("collapsed");
			$("#view_button").addClass("full");
			$("#view_button span").html("chevron_left")
		}
	})

	$(document).on("click","#sidebar .menu .button",function(){
		var menu = $(this).attr("id").replace("_button","");
		
		removeSelectorClassByPrefix("#sidebar", "selected_")
		$("#sidebar").addClass("selected_" + menu);
		
		var menu_section = {};
		menu_section['menu'] = menu;

		setURLParameters(menu_section);
		loadPage(menu);
	})

	

	$(document).on("click","#pane #back",function(){
		var url = getCurrentURL();
		
		var menu_section = {};
			
			
		switch(url['type']){
			case "activity":
			case "exhibit":
				menu_section['menu'] = "activities"
				break;

			case "program":
				menu_section['menu'] = "programs"
				break;

			case "display":
				menu_section['menu'] = "displays"
				break;
		}

		if(url['mode'] == "edit" && url['type'] != undefined){

			setURLParameters(menu_section,url['type'],"view",url['id']);	
			loadPage("view_" + url['type']);

		} else 

		if (url['mode'] == "view"){
			

			setURLParameters(menu_section);	
			loadPage(menu_section['menu']);
		} else
		
		if (url['mode'] == undefined && url['menu'] != undefined){
			var menu = url['menu'].split("/")
		
			var menu_section = {};
			menu_section['menu'] = menu[0];

			setURLParameters(menu_section);	
			loadPage(menu_section['menu']);
		} else

		if (url['mode'] != undefined && url['menu'] != undefined){
			var menu = url['menu'].split("/")
		
			var menu_section = {};
			menu_section['menu'] = menu[0];

			setURLParameters(menu_section);	
			loadPage(menu_section['menu']);
		}

		
		
		
		
	});

	$(document).on("click","#sidebar .logo",function(){
		
		var menu_section = {};
		menu_section['menu'] = "displays";

		setURLParameters(menu_section);	
		loadPage("displays");
		
		removeSelectorClassByPrefix("#sidebar", "selected_")
		$("#sidebar").addClass("selected_displays");

	});

	$(document).on("click","#view_hidden",function(){
		
		if($("#pane .list_content").hasClass("hidden_selected")){
			
			$("#pane .list_content").removeClass("hidden_selected");
			$("#pane .list_content").addClass("visible_selected");

			$("#view_hidden .icon span").html("visibility_off");
			$("#view_hidden .text").html("Hide Hidden")


		} else 

		if($("#pane .list_content").hasClass("visible_selected")){

			$("#pane .list_content").removeClass("visible_selected");
			$("#pane .list_content").addClass("hidden_selected");

			$("#view_hidden .icon span").html("visibility");
			$("#view_hidden .text").html("View All")

		}
	});

	$(document).on("click","#create_display",function(){
		var menu_section = {};
		menu_section['menu'] = "displays";

		setURLParameters(menu_section,"display","create");	
		loadPage("create_display");
	});

	$(document).on("click","#create_program",function(){
		var menu_section = {};
		menu_section['menu'] = "programs";

		setURLParameters(menu_section,"program","create");	
		loadPage("create_program");
	});

	$(document).on("click","#create_exhibit",function(){
		var menu_section = {};
		menu_section['menu'] = "activities";

		setURLParameters(menu_section,"exhibit","create");	
		loadPage("create_exhibit");
	});

	$(document).on("click","#create_activity",function(){
		var menu_section = {};
		menu_section['menu'] = "activities";

		setURLParameters(menu_section,"activity","create");	
		loadPage("create_activity");
	});

	$(document).on("click",".action_button",function(){
		var action = $(this).attr("data-action");
		var id;

		var url = getCurrentURL();

		if (url['id'] != undefined){
			id = url['id'];
		} else

		if ($(this).attr("data-id") != undefined){
			id = $(this).attr("data-id");
		} 

		else {
			id = 0;
		}

		console.log(id)

		switch(action){
			case "view_display":
				var menu_section = {};
				menu_section['menu'] = "displays"

				setURLParameters(menu_section,"display","view",id);	
				loadPage("view_display");

				break;

			case "edit_display":
				var menu_section = {};
				menu_section['menu'] = "displays";

				setURLParameters(menu_section,"display","edit",id);	
				loadPage("edit_display");

				break;

			case "download_display":
				alert("Downloading Display")
				break;

			case "preview_display":
				alert("Previewing Display")
				break;

			case "archive_display":
				alert("Archiving Display")
				break;

			case "delete_display":
				alert("Deleting Display")
				break;

			case "save_display":
				alert("Save Display")
				break;

			case "view_program":
				var menu_section = {};
				menu_section['menu'] = "programs";

				setURLParameters(menu_section,"program","view",id);	
				loadPage("view_program");
				break;

			case "edit_program":
				var menu_section = {};
				menu_section['menu'] = "programs";


				setURLParameters(menu_section,"program","edit",id);	
				loadPage("edit_program");
				break;

			case "archive_program":
				alert("Archiving Program")
				break;

			case "delete_program":
				alert("Deleting Program")
				break;

			case "save_program":
				alert("Saving Program")
				break;

			case "view_activity":
				var menu_section = {};
				menu_section['menu'] = "activities";

				setURLParameters(menu_section,"activity","view",id);	
				loadPage("view_activity");
				break;

			case "edit_activity":
				var menu_section = {};
				menu_section['menu'] = "activities";

				setURLParameters(menu_section,"activity","edit",id);	
				loadPage("edit_activity");
				break;

			case "preview_activity":
				alert("Previewing Activity")
				break;

			case "archive_activity":
				alert("Archiving Activity")
				break;

			case "delete_activity":
				alert("Deleting Activity")
				break;

			case "save_activity":
				alert("Saving Activity")
				break;

			case "view_exhibit":
				var menu_section = {};
				menu_section['menu'] = "activities";

				setURLParameters(menu_section,"exhibit","view",id);	
				loadPage("view_exhibit");
				break;

			case "edit_exhibit":
				var menu_section = {};
				menu_section['menu'] = "activities";

				setURLParameters(menu_section,"exhibit","edit",id);	
				loadPage("edit_exhibit");
				break;

			case "preview_exhibit":
				alert("Previewing Exhibit")
				break;

			case "archive_exhibit":
				alert("Archiving Exhibit")
				break;

			case "delete_exhibit":
				alert("Deleting Exhibit")
				break;

			case "save_exhibit":
				alert("Saving Exhibit")
				break;

			case "cancel_edit":
				var menu_section = {};
				menu_section['menu'] = url['menu'];

				setURLParameters(menu_section,url["type"],"view",url['id']);	
				loadPage("view_" + url['type']);
				break;


			case "menu":
				var loadSection = $(this).parent(".action_card").attr("data-section");
				
				var menu_section = {};
				menu_section['menu'] = url['menu'];
				menu_section['tier1'] = loadSection;

				setURLParameters(menu_section);	
				loadPage( url["menu"] + "/" + loadSection );
				
				break;

			
			
			

			
		}
	});

	$(document).on("click",".system_button",function(){
		var action = $(this).attr("data-action");
		var id;

		var url = getCurrentURL();

		var menu_section = {};
		menu_section['menu'] = url['menu'];
		menu_section['tier1'] = action;

		setURLParameters(menu_section);	
		loadPage(url['menu'] + "/" + action);

			
			
			

			
		
	});

	$(document).on("change",".form_checkbox",function(){

		var section = $(this).attr("data-section");

		if($(this).prop("checked")){
			$("#" + section).removeClass("hide")
		} else {
			$("#" + section).addClass("hide")
		}
	});
})

/* ==========================================================================
   URL Functions
   ========================================================================== */

//Returns an array with various information about the current URL.
function getCurrentURL(param) {

	//Constructs a full URL.
	var self = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search;
	var parent = window.parent.location.protocol + "//" + window.parent.location.host + "/" + window.parent.location.pathname + window.parent.location.search;
	var hash = window.location.hash;

	var url = "";

	//Check if param is set to "self" or "parent".
	//Useful for getting the URL of the parent window while in a menu frame.
	if (param == undefined || param == "self") {
		url = self;
	} else if (param == "parent") {
		url = parent;
	}


	var getUrl = window.location;
	var baseURL = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname;

	//Intializt the return array.
	var urlArray = {
		"url": url,
		"base": baseURL,
		"hash": hash
	};

	//Creates an array with any parameters that are found in the URL
	var request = {};
	var pairs = url.substring(url.indexOf('?') + 1).split('&');
	for (var i = 0; i < pairs.length; i++) {
		if (!pairs[i])
			continue;
		var pair = pairs[i].split('=');
		request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
	}

	//Combines the request array with urlArray.
	$.each(request, function(index, value) {
		urlArray[index] = value;
	})


	return urlArray;
}

//This function manages the URL parameters that are used in the interface.  
//It updates the URL to reflect the current location of the user without reloading the page.
function setURLParameters(menu_section,type,mode,id) {

	var url = getCurrentURL();

	var new_params = {};
	var menu_query = [];

	//Check to see if the menu section is defined.
	if (menu_section != undefined && menu_section != "") {

		//If menu is defined, but tier1 is not defined, we will just set the new parameters to the menu section.  
		if (menu_section['menu'] != undefined && menu_section['tier1'] == undefined) {

			new_params['menu'] = menu_section['menu'];

		}

		//If menu and tier1 are both defined, we will merge them with a forward slash before setting the new parameters.
		else if (menu_section['menu'] != undefined && menu_section['tier1'] != undefined) {

			new_params['menu'] = menu_section['menu'] + '/' + menu_section['tier1'];

		} else if (menu_section['menu'] == undefined && menu_section['tier1'] == undefined) {
			
			new_params['menu'] = "displays";

		}

	}


	if (mode == "view" || mode == "edit" || mode == "create") {

		new_params['mode'] = mode;

	} 

	if(type != undefined){
		new_params['type'] = type;
	}

	if(id != undefined){
		new_params['id'] = id;
	}

	//Take this new parameters and format them for use in a URL by joining each parameter with an equal sign.
	$.each(new_params, function(index, value) {
		menu_query.push(index + '=' + value);
	});

	//Join all of the parameters together using an ampersand.
	var final_query = menu_query.join('&');

	//Push the final URL to the window without reloading.
	var final_url = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + final_query;
	window.history.pushState({
		path: final_url
	}, '', final_url);

}

//Get an HTML file with AJAX and return its content.
function loadHTML(file) {

	//Use Ajax to load HTML content.
	var fileHTML = $.ajax({
		url: file,
		async: false
	}).responseText;

	return fileHTML;

}

function loadPage(page) {
	var url = getCurrentURL();
	var file = url['base'] + "pages/" + page + ".php";
	var data = loadHTML(file)
	
	$("#pane").html(data);

	//Initialize Select2 to style the dropdown menus.
	$('.dropdown').select2({
		minimumResultsForSearch: Infinity,
		width: '400px'
	})

}

/* ==========================================================================
   Class Functions
   ========================================================================== */

//Accepts a prefix and removes all classes from body that fit.
function removeBodyClassByPrefix(prefix) {
	var regex = new RegExp("(^|\\s)" + prefix + "\\S+", 'g')

	$('body').removeClass(function(index, className) {
		return (className.match(regex) || []).join(' ');
	});
}

//Accepts a prefix and removes all classes from the selector that fit.
function removeSelectorClassByPrefix(selector, prefix) {
	var regex = new RegExp("(^|\\s)" + prefix + "\\S+", 'g')

	$(selector).removeClass(function(index, className) {
		return (className.match(regex) || []).join(' ');
	});

}

/* ==========================================================================
   LocalStorage Functions
   ========================================================================== */

//Gets a string from localStorage.
function getLocal(name, setting_default) {

	var value = localStorage.getItem(name);

	if (value == null) {
		value = setting_default;
	}

	return value;

}

//Gets an object from localStorage
function getLocalObject(name, setting_default) {

	var value = localStorage.getItem(name);
	var deStrify = JSON.parse(value)

	if (deStrify == null) {
		deStrify = setting_default;
	}

	return deStrify;

}

//Sets a string in localStorage.
function setLocal(name, user_setting) {
	localStorage.setItem(name, user_setting)
}

//Sets an object from localStorage
function setLocalObject(name, user_setting) {
	var JSONStringify = JSON.stringify(user_setting);
	localStorage.setItem(name, JSONStringify)
}

/* ==========================================================================
   SessionStorage Functions
   ========================================================================== */

//Gets a string from sessionStorage.
function getSession(name, setting_default) {

	var value = sessionStorage.getItem(name);

	console.log(name)
	if (value == null) {
		value = setting_default;
	}

	return value;
}

//Gets an object from sessionStorage.
function getSessionObject(name, setting_default) {

	var value = sessionStorage.getItem(name);
	var deStrify = JSON.parse(value)

	if (deStrify == null) {
		deStrify = setting_default;
	}

	return deStrify;
}

//Sets a string in sessionStorage.
function setSession(name, user_setting) {
	sessionStorage.setItem(name, user_setting)
}

//Sets an object in sessionStorage.
function setSessionObject(name, user_setting) {
	var JSONStringify = JSON.stringify(user_setting);
	sessionStorage.setItem(name, JSONStringify)
}
