# CLIO Create

[CLIO Create](https://gitlab.com/erismuseums/clio-create) is a web application to create and manage interactive activities for use with digital exhibits. Using [CLIO Exhibit](https://gitlab.com/erismuseums/clio-exhibit), these activities can be used in online exhibits, video conferences and physical kiosks.

## Installation

**CLIO Create** requires a web server with PHP v5.6 or newer.

Installation guides can be found on our wiki [here](https://www.cliomuseums.org/wiki).

## Usage

Once the **CLIO Create** web application has been installed in the web server's host directory, it can be accessed through the *localhost* or *127.0.0.1* web address using a web browser.

## Contributing
Bug reports and feature requests are welcome.

## License
[MIT](https://choosealicense.com/licenses/mit/)